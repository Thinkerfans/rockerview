package com.android.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.SurfaceView;

import com.rp.R;

public class RudderView1 extends SurfaceView implements Runnable, Callback{

	protected int mWheelResId = R.drawable.center_button;
	protected int mRudderResId = R.drawable.ptz_4;

	protected int mRudderRadius = 20;// 摇杆半径
	protected int mWheelRadius = 60;// 摇杆活动范围半径

	protected SurfaceHolder mHolder;

	protected boolean isStop = false;

	protected Thread mThread;
	protected Paint mPaint;

	protected Point mWheelCenter = new Point(80, 80);// 摇杆起始位置
	protected Point mRudderPos; // 摇杆位置

	private RudderListener mlistener = null; // 事件回调接口

	protected Bitmap mWheelBitmap;
	protected Bitmap mRudderBitmap;

	// 回调接口
	public interface RudderListener {
		void onSteeringWheelChanged(int angle);
	}

	public RudderView1(Context context) {
		super(context);
		init();
	}

	private void init() {
		this.setKeepScreenOn(true);
		setFocusable(true);
		setFocusableInTouchMode(true);
		setZOrderOnTop(true);

		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setFormat(PixelFormat.TRANSPARENT);// 设置背景透明

		mPaint = new Paint();
		mPaint.setAntiAlias(true);// 抗锯齿
		mRudderPos = new Point(mWheelCenter);
	}

	public RudderView1(Context context, AttributeSet as) {
		super(context, as);
		init();
		LogUtil.e("RudderView1 : RudderView1 ", "x :" + this.getX() + " Y :"
				+ this.getY());
	}

	// 设置回调接口
	public void setRudderListener(RudderListener rockerListener) {
		mlistener = rockerListener;
	}

	@Override
	public void run() {
		initBitmap();
		Canvas canvas = null;
		while (!isStop) {
			try {
				canvas = mHolder.lockCanvas();
				if (canvas != null) {
					canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);// 清除屏幕
					canvas.drawBitmap(mRudderBitmap, mWheelCenter.x - mWheelRadius,
							mWheelCenter.y - mWheelRadius, mPaint);// 绘制范围
					canvas.drawBitmap(mWheelBitmap, mRudderPos.x
							- mRudderRadius, mRudderPos.y - mRudderRadius,
							mPaint);// 绘制控制按钮
					mHolder.unlockCanvasAndPost(canvas);
				}
				
				Thread.sleep(30);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		LogUtil.e("RudderView1 : surfaceCreated ", "x :" + this.getWidth() + " Y :"
				+ this.getHeight());
		mThread = new Thread(this);
		mThread.start();

	}
	
	protected void initBitmap(){
		mWheelBitmap = BitmapFactory.decodeResource(getResources(),
				mWheelResId);
		mWheelBitmap = Bitmap.createScaledBitmap(mWheelBitmap,
				mRudderRadius * 2, mRudderRadius * 2, true);
		mRudderBitmap = BitmapFactory.decodeResource(getResources(), mRudderResId);
		mRudderBitmap = Bitmap.createScaledBitmap(mRudderBitmap, mWheelRadius * 2,
				mWheelRadius * 2, true);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		isStop = true;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int len = MathUtils.getLength(mWheelCenter.x, mWheelCenter.y,
				event.getX(), event.getY());

		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// 如果屏幕接触点不在摇杆挥动范围内,则不处理
			if (len > mWheelRadius) {			
				return true;
			}
		}
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			if (len <= mWheelRadius) {
				// 如果手指在摇杆活动范围内，则摇杆处于手指触摸位置
				mRudderPos.set((int) event.getX(), (int) event.getY());

			} else {
				// 设置摇杆位置，使其处于手指触摸方向的 摇杆活动范围边缘
				mRudderPos = MathUtils.getBorderPoint(mWheelCenter, new Point(
						(int) event.getX(), (int) event.getY()), mWheelRadius);
			}
			
			if (mlistener != null) {
				float radian = MathUtils.getRadian(mWheelCenter, new Point(
						(int) event.getX(), (int) event.getY()));
				mlistener.onSteeringWheelChanged(MathUtils.getAngleCouvert(radian));
			}
		}
		// 如果手指离开屏幕，则摇杆返回初始位置
		if (event.getAction() == MotionEvent.ACTION_UP) {
			mRudderPos = new Point(mWheelCenter);
		}
		return true;
	}

}
