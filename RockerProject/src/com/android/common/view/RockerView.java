package com.android.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.View;

import com.rp.R;

public class RockerView extends View implements Runnable {

	private int mBgId = R.drawable.ptz_4;
	private static final String TAG = "Rocker";

	private Paint mPaint;
	private int mWidth;
	private int mHeight;

	public RockerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		mPaint = new Paint();
		new Thread(this).start();
	}

	public RockerView(Context context) {
		super(context);
		init();
	}

	private void drawBackground(Canvas canvas) {
		// mCanvas.drawColor(Color.TRANSPARENT,Mode.CLEAR);//清除屏幕
		Bitmap b = BitmapFactory.decodeResource(getResources(), mBgId);
		canvas.drawBitmap(b, 0, 0, null);

	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		drawBackground(canvas);
		
		
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			// 使用postInvalidate可以直接在线程中更新界面
			postInvalidate();
		}
	}

}
