package com.android.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.rp.R;

public class Rocker extends SurfaceView implements SurfaceHolder.Callback {

	private int mBgId = R.drawable.ptz_4;
	private int mFingerId = R.drawable.center_button;
	private static final String TAG = "Rocker";

	private SurfaceHolder mHolder;
	private Canvas mCanvas;

	private int mWidth, mHeight, mTop, mLeft;

	private Bitmap mFingerBitmap, mBgBitmap;

	public Rocker(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		setFocusable(true);
		setFocusableInTouchMode(true);
		mHolder = getHolder();
		mHolder.addCallback(this);

		mFingerBitmap = BitmapFactory.decodeResource(getResources(), mFingerId);

	}

	public Rocker(Context context) {
		super(context);
		init();
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {

		mWidth = this.getMeasuredWidth();
		mHeight = this.getHeight();
		mLeft = this.getLeft();
		mTop = this.getTop();
		LogUtil.e(TAG,
				"surfaceCreated : width :" + mWidth + " height: " + mHeight
						+ "top : " + this.getTop() + "left :" + this.getLeft());

	}

	private void drawBackground() {
		mCanvas = mHolder.lockCanvas();
		// mCanvas.drawColor(Color.TRANSPARENT,Mode.CLEAR);//清除屏幕
		Bitmap b = BitmapFactory.decodeResource(getResources(), mBgId);
		mCanvas.drawBitmap(b, 0, 0, null);
		try {
			if (mCanvas != null)
				mHolder.unlockCanvasAndPost(mCanvas);
		} catch (Exception e2) {

		}
	}

	private Bitmap createFramedPhoto() {

		Bitmap output = Bitmap.createBitmap(mFingerBitmap.getWidth(),
				mFingerBitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, mFingerBitmap.getWidth(),
				mFingerBitmap.getHeight());

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(mFingerBitmap.getWidth() / 2,
				mFingerBitmap.getHeight() / 2, mFingerBitmap.getWidth() / 2,
				paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(mFingerBitmap, rect, rect, paint);
		return output;

		// Rect dst = new Rect(0, 0,mWidth, mHeight);
		// Bitmap output = Bitmap.createBitmap(mWidth,mHeight,Config.ARGB_8888);
		// Canvas canvas = new Canvas(output);
		// Paint paint = new Paint();
		// paint.setAntiAlias(true);
		// canvas.drawOval(new RectF(0, 0,mWidth, mHeight), paint);
		// paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// //绘制图片的时候，给画笔加一个灰色的蒙层
		// // if(colorFilter != null&&isClickable)
		// // paint.setColorFilter(colorFilter);
		// canvas.drawBitmap(mBgBitmap, null, dst, paint);
//		return output;

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {

	}

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);

//		if (mBgBitmap == null) {
//			Drawable drawable = getBackground();
//			if (drawable != null) {
//				mBgBitmap = ((BitmapDrawable) drawable).getBitmap();
//			}
//			if (mBgBitmap == null || mBgBitmap.isRecycled()) {
//				return;
//			}
//			canvas.drawBitmap(createFramedPhoto(), 0, 0, new Paint());
//		}
		//
		 mCanvas = mHolder.lockCanvas();
		 mCanvas.drawBitmap(createFramedPhoto(), 0, 0, null);
		 try {
		 if (mCanvas != null)
		 mHolder.unlockCanvasAndPost(mCanvas);
		 } catch (Exception e2) {
		
		 }
	}

}
