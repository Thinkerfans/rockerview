package com.android.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.SurfaceView;

import com.rp.R;

public class RudderView2 extends RudderView1{
	
	 int mRudderResId = R.drawable.bg_ptz_8;

	public RudderView2(Context context, AttributeSet as) {
		super(context, as);
	}

	@Override
	protected void initBitmap() {
		mWheelBitmap = BitmapFactory.decodeResource(getResources(),
				mWheelResId);
		mWheelBitmap = Bitmap.createScaledBitmap(mWheelBitmap,
				mRudderRadius * 2, mRudderRadius * 2, true);
		mRudderBitmap = BitmapFactory.decodeResource(getResources(), mRudderResId);
		mRudderBitmap = Bitmap.createScaledBitmap(mRudderBitmap, mWheelRadius * 2,
				mWheelRadius * 2, true);
		
	}
	
	
	

}
